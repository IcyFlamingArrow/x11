# Copyright 2019 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mesa.exlib' which is:
# Copyright 2008 Alexander Færøy <eroyf@eroyf.org>
# Copyright 2008-2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>

require gitlab [ prefix=https://gitlab.freedesktop.org user=glvnd tag=v${PV} new_download_scheme=true ]
require meson
require xdummy [ phase=test option=X ]
require alternatives

SUMMARY="Vendor-neutral dispatch layer for arbitrating OpenGL API calls between multiple vendors"
DESCRIPTION="
libglvnd allows multiple drivers from different vendors to coexist on the same filesystem,
and determines which vendor to dispatch each API call to at runtime.
Both GLX and EGL are supported, in any combination with OpenGL and OpenGL ES.
"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    X [[ presumed = true ]]
    ( libc: musl )
"

DEPENDENCIES="
    build:
        dev-lang/python:*
        virtual/pkg-config
        X? ( x11-proto/xorgproto )
    build+run:
        X? (
            x11-libs/libX11
            x11-libs/libXext
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-symbols-check-disregard-platform-symbols-from-compil.patch
)

MESON_SOURCE=${WORKBASE}/${PN}-v${PV}

MESON_SRC_CONFIGURE_PARAMS=(
    -Degl=true
    -Dgles1=true
    -Dgles2=true
    -Dentrypoint-patching=disabled
    -Dheaders=true
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'X x11'
    'X glx'
    '!libc:musl tls'
)

src_test() {
    option X && xdummy_start

    meson_src_test

    option X && xdummy_stop
}

# In an ideal world, none of this would be needed, but nvidia's legacy drivers for older GPUs do not
# support GLVND, so we need to keep the alternative for as long as we support those drivers.
# To make matters worse, the "newer" of the legacy branches bundles libglvnd, while still needing
# the alternative for xorg's libglx.so module, so we need to alternative everything.
# However, only people that use these drivers will have an opengl alternative besides "glvnd"
src_install() {
    local host=$(exhost --target)

    meson_src_install

    # alternatives
    local libs=(libEGL libGLESv1_CM libGLESv2 libOpenGL libGLdispatch)
    option X && libs+=(libGL libGLX)

    edo mkdir "${IMAGE}"/usr/${host}/lib/opengl/

    local lib path_old path_new soname
    for lib in "${libs[@]}"; do
        path_new=/usr/${host}/lib/opengl/${lib}-glvnd.so
        path_old=$(readlink -f "${IMAGE}"/usr/${host}/lib/${lib}.so)

        edo mv \
            "${path_old}" \
            "${IMAGE}"/${path_new}

        local objdump=$(exhost --tool-prefix)objdump
        soname=$(edo ${objdump} -p "${IMAGE}"/${path_new} | sed -n 's/^ *SONAME *//p')

        # clean up the leftover symlinks
        edo rm "${IMAGE}"/usr/${host}/lib/{${lib}.so,${soname}}

        alternatives+=(
            /usr/${host}/lib/${lib}.so ${path_new}
            /usr/${host}/lib/${soname} ${path_new}
        )
    done

    alternatives_for opengl glvnd 100 ${alternatives[@]}
}

