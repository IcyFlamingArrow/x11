# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2015 Thomas G. Anderson <tanderson@caltech.edu>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_configure

SUMMARY="The fluxbox window manager"
DESCRIPTION="
Fluxbox is a windowmanager for X that was based on the Blackbox 0.61.1 code. It
is very light on resources and easy to handle but yet full of features to make
an easy, and extremely fast, desktop experience. Fluxbox is supplied with lots
of native options such as tabbing, grouping, a long list of keycommands,
Chainable Keygrabber, fully editable menu and much, much more
"
HOMEPAGE="http://fluxbox.org/"
LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    bidi [[ description = [ support bidirectional text through fribidi ] ]]
    imlib [[ description = [ Use imlib2 for drawing icons ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXft
        x11-libs/libXinerama
        x11-libs/libXrandr[>=1.2]
        x11-libs/libXrender
        x11-libs/libXpm
        bidi? ( dev-libs/fribidi )
        imlib? ( media-libs/imlib2 )
    run:
        x11-apps/xmessage
"

REMOTE_IDS="freecode:fluxbox"
UPSTREAM_CHANGELOG="http://git.fluxbox.org/fluxbox.git/plain/ChangeLog"
UPSTREAM_RELEASE_NOTES="http://git.fluxbox.org/fluxbox.git/plain/NEWS"

DEFAULT_SRC_COMPILE_PARAMS=( AR=${AR} )

fluxbox_src_configure() {
    econf --enable-nls                                      \
          --enable-xrandr                                   \
          --enable-xinerama                                 \
          --with-apps=/usr/share/fluxbox/apps               \
          --with-init=/usr/share/fluxbox/init               \
          --with-keys=/usr/share/fluxbox/keys               \
          --with-locale=/usr/share/fluxbox/nls              \
          --with-menu=/usr/share/fluxbox/menu               \
          --with-overlay=/usr/share/fluxbox/apps            \
          --with-style=/usr/share/fluxbox/styles/bloe       \
          --with-windowmenu=/usr/share/fluxbox/windowmenu   \
          $(option_enable bidi fribidi)                     \
          $(option_enable imlib imlib2)                     \
          $(expecting_tests && echo --enable-tests)
}

