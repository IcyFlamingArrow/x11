# Copyright 2013-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt [ docs=false ] qmake [ slot=5 ]

export_exlib_phases src_configure src_install

SUMMARY="Qt Cross-platform application framework: QtTools"
DESCRIPTION="
Qt Assistant: A Tool for viewing the Qt documentation
Qt Designer: Designing and building graphical user interfaces in a visual editor
Qt Linguist: Contains tools for the roles typically involved in localizing applications
D-Bus Viewer: A tool to introspect D-Bus objects and messages.
"

MYOPTIONS="
    examples
    gui [[ description = [ Build GUI tools ]
        presumed = true
    ]]
    webkit [[ description = [ Use QtWebkit to display help and for QtDesigner plugins ]
        requires = gui
    ]]
"

DEPENDENCIES="
    build+run:
        dev-lang/clang:=[>=3.9.0]
        dev-lang/llvm:=[>=3.9.0]
        gui? (
            x11-libs/qtbase:${SLOT}[>=${PV}][gui(+)][sql][sqlite] [[ note = [ assistant ] ]]
            x11-libs/qtdeclarative:${SLOT}[>=${PV}]               [[ note = [ linguist ] ]]
        )
        !gui? (
            x11-libs/qtbase:${SLOT}[>=${PV}][?gui(+)][sql][sqlite]
        )
        webkit? (
            x11-libs/qtwebkit:${SLOT}[>=5.5.0]   [[ note = [ assistant, designer ] ]]
        )
"

qttools_src_configure() {
    # No nicer way to do this unfortunately
    option webkit || edo sed -e "s/qtHaveModule(webkitwidgets)/false/" \
        -i src/assistant/assistant/assistant.pro \
        -i src/designer/src/plugins/plugins.pro

    if ! option gui ; then
        edo sed -e "s/qtHaveModule(widgets)/false/" \
            -i examples/examples.pro \
            -i src/linguist/linguist.pro \
            -i src/qdbus/qdbus.pro \
            -i src/qtdiag/qtdiag.pro \
            -i src/src.pro \
            -i tests/auto/cmake/cmake.pro

        edo sed -e "s/qtHaveModule(qmldevtools-private)/false/" \
            -i src/linguist/lupdate/lupdate.pro \
            -i src/qdoc/qdoc.pro

        edo sed -e "s/qtHaveModule(gui)/false/" \
            -i src/src.pro

        edo sed -e "s/qtHaveModule(quickwidgets)/false/" \
            -i src/designer/src/plugins/plugins.pro
    fi

    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qttools_src_install() {
    default

    local host=$(exhost --target)
    # They collide with the versions from x11-libs/qt:4
    local colliding_binaries=( lconvert lrelease lupdate qdbus )

    if option gui ; then
        colliding_binaries+=( assistant designer linguist pixeltool
            qcollectiongenerator qdbusviewer qhelpgenerator )
    fi

    edo mkdir -p "${IMAGE}"/usr/${host}/bin
    for i in ${colliding_binaries[@]} ; do
        [[ -e "${IMAGE}"/usr/${host}/lib/qt5/bin/${i} ]] || die "/usr/${host}/lib/qt5/bin/${i} does not exist in ${IMAGE}"
        dosym /usr/${host}/lib/qt5/bin/${i} /usr/${host}/bin/${i}-qt5
    done

    local binaries=( qdoc qtattributionsscanner qtpaths qtplugininfo )

    if option gui ; then
        binaries+=( qtdiag )
    fi

    for i in ${binaries[@]} ; do
        [[ -e "${IMAGE}"/usr/${host}/lib/qt5/bin/${i} ]] || die "/usr/${host}/lib/qt5/bin/${i} does not exist in ${IMAGE}"
        dosym /usr/${host}/lib/qt5/bin/${i} /usr/${host}/bin/${i}
    done

    # remove references to build dir
    if option gui ; then
        edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/${host}/lib/libQt5*.prl
    fi
}

